﻿using System;
using System.Runtime.InteropServices;

namespace tacofetch
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine(System.Runtime.InteropServices.RuntimeInformation.OSDescription);

            var OS = SysFetch.GetOsPlatform();
            if (OS == OSPlatform.Linux)
            {
                Console.WriteLine("This is linux!");
            }
        }
    }
}