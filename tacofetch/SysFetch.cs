using System.Runtime.InteropServices;

namespace tacofetch
{
    public class SysFetch
    {
        public string OSString;

        public SysFetch()
        {
            
        }

        public static OSPlatform GetOsPlatform()
        {
            var osBsd = OSPlatform.Create("BSD");
            var osUnknown = OSPlatform.Create("UNKNOWN");

            if (IsLinux())
            {
                return OSPlatform.Linux;
            }

            if (IsMac())
            {
                return OSPlatform.OSX;
            }

            if (IsWindows())
            {
                return OSPlatform.Windows;
            }

            if (IsBsd())
            {
                return osBsd;
            }

            return osUnknown;
        }

        private static bool IsWindows() =>
            RuntimeInformation.IsOSPlatform(OSPlatform.Windows);

        private static bool IsMac() =>
            RuntimeInformation.IsOSPlatform(OSPlatform.OSX);

        private static bool IsLinux() =>
            RuntimeInformation.IsOSPlatform(OSPlatform.Linux);

        private static bool IsBsd()
        {
            return false;
        }
    }
}